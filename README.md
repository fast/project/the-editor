# THE EDITOR

This is the repository for THE EDITOR -- a small wrapper around [tiptap](https://www.tiptap.dev/) that provides some default configuration, a toolbar, and a few other things.

## Getting Started
There's 2 ways to use THE EDITOR -- either by manually copying the files to your project to be used in a `<script type="module">` tag, or by installing it as an npm package in a vite / node project.

### Via NPM
To use THE EDITOR with NPM, you'll need to add the `@fast` registry to your project:
    
```bash
# In the same folder as package.json
echo @fast:registry=https://git.uwaterloo.ca/api/v4/packages/npm/ >> .npmrc
# or via npm config:
# npm config set @fast:registry=https://git.uwaterloo.ca/api/v4/packages/npm/

npm install @fast/the-editor
```

You can then easily use the component:
    
```vue
<template>
  <the-editor v-model="content" />
</template>

<script setup>
    import { TheEditorComponent as TheEditor } from '@fast/the-editor'
    import { ref } from 'vue'
    // You'll need to import the styles as well!
    import '@fast/the-editor/dist/styles-te.css' // Styles for the editor
    import '@fast/the-editor/dist/styles-tec.css' // Styles for the editor content

    const content = ref('<h1>Hello World!</h1>')
</script>
```

### Manual usage (es module)
Modern browsers now support es modules, so you can use THE EDITOR by copying the files into your project and using it in a `<script type="module">`. Copy the files from `/dist` into your project, and then use it like so (ensure you use an import map to put `vue` into scope!):


```html
<style>
    @import url('/static/the-editor/styles-te.css');
    @import url('/static/the-editor/styles-tec.css');
</style>

<div id="app">
    <the-editor v-model="content" />
</div>

<script type="importmap">
    {
        "imports": {
            "vue": "/static/vue.esm-browser.js",
            "the-editor": "/static/the-editor/the-editor.js"
        }
    }
</script>
<script type="module">
    import { TheEditorComponent as TheEditor } from 'the-editor'
    import { createApp } from 'vue'
    const app = createApp({
        components: {
            TheEditor
        },
        data() {
            return {
                content: '<h1>Hello World!</h1>'
            }
        }
    })
    app.mount('#app')
</script>
```


## Configuration
THE EDITOR comes with a bunch of extensions enabled by default, and includes shortcuts, keymaps, and a toolbar. These are based on 

- Bold
- Italic
- Strike-through
- Underline
- Superscript and Subscript
- Lists (Bullet and Numbered)
- Headers (1-4)
- Paragraph
- Code blocks
- Text aligning
- Horizontal Rule
- Links
- Tables

You can see all of the available extensions in the [TipTap documentation](https://tiptap.dev/introduction).

By design the TipTap & Prosemirror libraries are very modular, and all we've done is bundled a bunch of extensions together and added a toolbar.

### Re-arranging the Toolbar & Removing Controls
If you're not a fan of the toolbar's default ordering, you can simply re-arrange or remove the order in which **controls** are declared. To do this, you can pass a new `layout` prop to the component.

The default layout string is: 

```
history | styleSelector | bold italic underline strike superscript subscript | bulletList orderedList blockquote | textAlign | table link
```

If you want to, for instance, remove the `styleSelector` and `history` controls, simply remove these from the string.

> Note: Removing controls from the layout will not disable the extensions associated with them. For example, if you remove the `link` control, you can still create links by pasting them into the editor.


### Adding or Overriding Controls
Let's say you'd like to create a new button that utilizes **existing editor functionality**, but would like to compose those functions into a single button.

The following code will add a button to the toolbar which inserts the text "Hello World!" into the editor at the cursor when clicked:

```vue
<template>
  <the-editor v-model="content" :layout="layout" :add_bar_controls="add_controls" />
</template>

<script setup>
import {TheEditorComponent as TheEditor, makeButton, defaultBarLayout} from '@fast/the-editor'
import { ref, h } from 'vue'

const content = ref('<h1>Hello World!</h1>')

// Use the makeButton utility class to create a new button!
const helloWorldButton = makeButton({
    content: h('b', 'HW')
    title: "Insert 'Hello World!'",
    active: ()=>false, // Whether to highlight the button as active
    enabled: ()=>true, // Whether the button is enabled and can be clicked
    action: (editor)=>{ // The action to perform when the button is clicked
        editor.chain().focus().insertContent('Hello World!').run()
    }
})

// register the control with a name we can reference in the layout:
const add_controls = {'helloWorld': helloWorldButton}

// Update the layout with our new control:
const layout = ref(defaultBarLayout + '| helloWorld')
</script>
```

You can also replace an existing control by using the same name as an existing control. For example, if you wanted to replace the separator, you could override the `|` control to be a span containing a dash:

```js
const add_bar_controls = {
    '|': {render: ()=>h('span', '-')}, // New component to render
    'helloWorld': helloWorldButton 
}
```



### Disabling Extensions
You can disable plugins by passing an array of names to the `disable_default_extensions` prop. For example, to disable the bold and italic plugins, you can do:

```vue
<template>
  <the-editor v-model="content" :disable_default_extensions="['bold', 'italic']" />
</template>
```

Disabling an extension does a couple things:
- It removes any shortcuts or keymaps associated with that extension
- It removes any controls from the toolbar
- It prevents related nodes from being pasted into the editor (e.g. if you disable the `link` extension, pasted links will be converted to plain text)

### Adding Extensions
Normal TipTap extensions can be added to the editor by passing them to the `extensions` prop. For example, if you'd like to add some text color options to the editor, you can install the TipTap extensions:

```bash
npm install @tiptap/extension-text-style @tiptap/extension-color
```
```js
import TextStyle from '@tiptap/extension-text-style'
import { Color } from '@tiptap/extension-color'

// Pass this to <the-edtior :extensions="extensions" .../>
const extensions = [TextStyle, Color]
```

Of course, TipTap itself is headless, so we could alternatively pass an object of the form `{extension: ext, controls: {...}}`

For example, we could add 2 buttons to make text RED or BLUE
    
```js
import TextStyle from '@tiptap/extension-text-style'
import { Color } from '@tiptap/extension-color'
import { makeButton, defaultBarLayout } from '@fast/the-editor'
import {h} from 'vue'

const redButton = makeButton({
    content: h('span', {style: 'color: red'}, 'R'),
    title: "Make text red",
    active: (editor)=>editor.isActive('textStyle', { color: '#FF0000' }),
    enabled: (editor)=>editor.can().setColor("#FF0000"),
    action: (editor)=>{
        editor.chain().focus().setColor("#FF0000").run()
    }
}) 

const blueButton = makeButton({
    content: h('span', {style: 'color: blue'}, 'B'),
    title: "Make text blue",
    active: (editor)=>editor.isActive('textStyle', { color: '#0000FF' }),
    enabled: (editor)=>editor.can().setColor("#0000FF"),
    action: (editor)=>{
        editor.chain().focus().setColor("#0000FF").run()
    }
})

// Pass this to <the-edtior :extensions="extensions" .../>
const extensions = [TextStyle, {extension: Color, controls: {red: redButton, blue: blueButton}}]
const layout = defaultBarLayout + '| red blue'
```

## Styling THE EDITOR
We've included some very barebones styles for THE EDITOR that should make due for simple applications, but you'll most likely want to customize the styles to fit your application.

We recommend you take a look at the default `style.css` included in the project, then copy and customize the provided styles in your own project stylesheets. We've tried to provide unique classes to relevant portions of THE EDITOR, so you should be able to override the styles easily.

## Advanced Usage

### Manually creating Bar / Editor contexts
TODO


### One bar, many editors

### Images
Oh boy oh boy, I sure do love images.

TipTap provides pretty sparse image support, so we've beefed up the default Image implementation with a couple of addons:

- Image resizing
- Wrapping images in a figure element
- Adding a caption and alt text to images
- Capturing paste events to automatically upload images

It was a lot of work, and so you're going to have to do a little legwork too to get things running smoothly. Here's a quick rundown of how to get started.

First you'll need 2 functions, `openImageDialog: (EditorInstance):void` and `uploadImage: Promise (File|string):string`.

openImageDialog can do whatever you want, but it should eventually call `editor.chain().focus().setImage({src: 'imageUrl', alt: 'An image', caption: 'A caption'}).run()`

`uploadImage` is a different beast. You'll need to write an uploadImage implementation that handles:
- Files
- data: urls
- normal urls

To make life slightly easier, we've provided some basic implementations of these functions that will get you started:


```js
// Image Example with Defaults
import {Image, imageControls} from './extensions/Image';
// Our default image upload function
import {}

```

#### Pasted Content
