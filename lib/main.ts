import TheEditorComponent from '../src/components/TheEditor.vue'
import TheEditBarComponent from '../src/components/TheEditBar.vue'
import useTheEditor, { TheEditorComponentProps } from '../src/useTheEditor'
import {TheEditor} from '../src/TheEditor'
import {defaultToolbarArray, defaultToolbarString} from '../src/defaultToolbar'
import {Editor as TipTapEditor, Extension, Node, Mark} from '@tiptap/core'
import ContentPlaceholderPlugin, {ContentPlaceholder, contentPlaceholderControls} from '../src/extensions/ContentPlaceholder'
import ImagePlugin, {Image, imageControls} from '../src/extensions/Image'
import EquationPlugin, {Equation, equationControls} from '../src/extensions/Equations'
import {inMemoryImageUploader, base64ImageUploader, serverImageUploader} from '../src/utilities/image_uploaders'
import ImageUploadDialog from '../src/components/ImageUploadDialog.vue'
import "../src/styles-te.css"
import makeButton from '../src/makeButton'

export {
    // Stuff for basic editing
    TheEditorComponent,
    TheEditBarComponent,
    TheEditor,
    useTheEditor,
    makeButton,
    defaultToolbarArray, defaultToolbarString,

    // Extra extensions

    ContentPlaceholderPlugin, ContentPlaceholder, contentPlaceholderControls,
    ImagePlugin, Image, imageControls,
    EquationPlugin, Equation, equationControls,
    ImageUploadDialog,
    inMemoryImageUploader,
    base64ImageUploader,
    serverImageUploader,

    // Stuff for extending the editor
    TipTapEditor, Extension, Node, Mark

}


export type {TheEditorComponentProps}
