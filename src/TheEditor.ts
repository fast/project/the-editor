import { Editor } from '@tiptap/vue-3'
import { Component } from 'vue'
import { Blockquote } from '@tiptap/extension-blockquote'
import { Bold } from '@tiptap/extension-bold'
import { BulletList } from '@tiptap/extension-bullet-list'
import { Code } from '@tiptap/extension-code'
import { CodeBlock } from '@tiptap/extension-code-block'
import { Document } from '@tiptap/extension-document'
import { Dropcursor } from '@tiptap/extension-dropcursor'
import { Gapcursor } from '@tiptap/extension-gapcursor'
import { HardBreak } from '@tiptap/extension-hard-break'
import { Heading } from '@tiptap/extension-heading'
import { History } from '@tiptap/extension-history'
import { HorizontalRule } from '@tiptap/extension-horizontal-rule'
import { Italic } from '@tiptap/extension-italic'
import { ListItem } from '@tiptap/extension-list-item'
import { OrderedList } from '@tiptap/extension-ordered-list'
import { Paragraph } from '@tiptap/extension-paragraph'
import { Strike } from '@tiptap/extension-strike'
import { Text } from '@tiptap/extension-text'
import { Tables } from './extensions/Tables'
import TextAlign from '@tiptap/extension-text-align'
import Underline from '@tiptap/extension-underline'
import SubScript from '@tiptap/extension-subscript'
import SuperScript from '@tiptap/extension-superscript'
import Link from '@tiptap/extension-link'
import { EditorOptions, AnyExtension } from '@tiptap/core'
import * as DC from './defaultControls'
import {defaultToolbarString} from './defaultToolbar'


export interface ExtensionWithControls {
    extension?: AnyExtension,
    controls: Record<string, Component>
}

export type TheEditorExtensionList = (AnyExtension | ExtensionWithControls)[]
type TheEditorExtensionRecord = Record<string, (AnyExtension | ExtensionWithControls)[]>

export interface TheEditorOptions extends Omit<EditorOptions, 'extensions'> {
    bar_layout: string|string[],
    disable_default_extensions: string[],
    enable_default_extensions: string[],
    extensions: TheEditorExtensionList,
}

export class TheEditor extends Editor {
    public bar_controls: Record<string, Component> = {}
    public bar_layout: string = defaultToolbarString
    constructor(options: Partial<TheEditorOptions>) {
        // Default extensions loaded into THE EDITOR
        let extensions_table: TheEditorExtensionRecord = {
            // Not going to enable removal of these.
            the_editor_defaults: [
                Document,
                Dropcursor,
                Gapcursor,
                HardBreak,
                Text,
                Paragraph,
            ],

            history: [{ extension: History, controls: DC.historyControls }],
            headings: [Heading],
            code: [CodeBlock, Code],

            bold: [{ extension: Bold, controls: DC.boldControls }],
            italic: [{ extension: Italic, controls: DC.italicControls }],
            underline: [{ extension: Underline, controls: DC.underlineControls }],
            strike: [{ extension: Strike, controls: DC.strikeControls }],
            subscript: [{ extension: SubScript, controls: DC.subscriptControls }],
            superscript: [{ extension: SuperScript, controls: DC.superscriptControls }],

            blockquote: [{ extension: Blockquote, controls: DC.blockquoteControls }],
            lists: [ListItem,
                { extension: BulletList, controls: DC.bulletListControls },
                { extension: OrderedList, controls: DC.orderedListControls },
            ],
            hr: [HorizontalRule],

            align: [{
                extension: TextAlign.configure({
                    types: ['heading', 'paragraph'],
                    alignments: ['left', 'center', 'right'],
                }),
                controls: DC.alignControls
            }],
            link: [{
                extension: Link.extend({
                    exitable: true,
                    inclusive: false,
                    addKeyboardShortcuts() {
                        return {
                            //@ts-ignore - We need to send some events down the editor pipe to be caught by the UI
                            'Mod-k': () => { this.editor.emit('customEvent:TriggerLinkEditor'); return true },
                        }
                    },
                }).configure({
                    openOnClick: false,
                }),
                controls: DC.linkControls
            }],
            tables: [{
                extension: Tables.configure({
                    table: { resizable: true },
                }),
                controls: DC.tableControls
            }],
            styleSelector: [{
                controls: DC.styleControls
            }]
        }

        let extensions: TheEditorExtensionList = []

        if (options.disable_default_extensions && options.enable_default_extensions){
            throw new Error('Cannot enable and disable default extensions at the same time')
        }

        if (options.disable_default_extensions) {
            // Remove the extensions that are disabled
            extensions_table = Object.keys(extensions_table).reduce((acc: TheEditorExtensionRecord, key) => {
                if (options.disable_default_extensions!.includes(key)) {
                    return acc
                }
                acc[key] = extensions_table[key]
                return acc
            }, {})
        }

        if (options.enable_default_extensions) {
            extensions_table = Object.keys(extensions_table).reduce((acc: TheEditorExtensionRecord, key) => {
                if (options.enable_default_extensions!.includes(key)||key==='the_editor_defaults') {
                    acc[key] = extensions_table[key]
                }
                return acc
            }, {})
        }
        extensions = Object.values(extensions_table).flat()
        extensions = extensions.concat(options.extensions || [])
        // split the extension collection into two parts: extensions and controls
        let { exts, comps } = extensions.reduce(
            (acc: { exts: AnyExtension[], comps: Record<string, Component> }, ex) => {
                // If this is an extension of type ExtensionWithControls, then we need to split it up
                if (ex.hasOwnProperty('controls')) {
                    if (ex.hasOwnProperty('extension')) {
                        acc.exts.push((<ExtensionWithControls>ex).extension!)
                    }
                    acc.comps = { ...acc.comps, ...(<ExtensionWithControls>ex).controls }
                } else {
                    acc.exts.push(<AnyExtension>ex)
                }
                return acc
            }, { exts: [], comps: {} })
        // REMOVE DUPLICATES, always keep the LAST entry
        // This allows for overriding default extensions
        exts = exts.reverse().filter((v, i, a) => a.findIndex(t => (t.name === v.name)) === i).reverse()
        super({ ...options, extensions: exts });
        this.bar_controls = {}
        this.registerBarControls(DC.spacerControls)
        this.bar_controls = comps

        if (options.bar_layout) {
            this.bar_layout = options.bar_layout instanceof Array ? options.bar_layout.join(' ') : options.bar_layout
        }
    }

    registerBarControls(controls: Record<string, Component>) {
        this.bar_controls = { ...this.bar_controls, ...controls }
    }

    unregisterBarControls(names: string[]) {
        names.forEach(name => {
            delete this.bar_controls[name]
        })
    }

}