import makeButton from "./makeButton"
import makeIcon from "./makeIcon"
import LinkEditTools from "./components/LinkEditTools.vue"
import HistoryButtons from "./components/HistoryButtons.vue"
import AlignButtons from "./components/AlignButtons.vue"
import StyleSelector from "./components/StyleSelector.vue"
import BarSpacer from "./components/BarSpacer.vue"
import TableEditTools from "./components/TableEditTools.vue"

export const spacerControls = {"|": BarSpacer}
export const historyControls = {history: HistoryButtons}
export const alignControls = {textAlign: AlignButtons}
export const styleControls = {styleSelector: StyleSelector}
export const linkControls = {link: LinkEditTools}
export const tableControls = {table: TableEditTools}
export const boldControls = {bold: makeButton({
    title: 'bold',
    shortcut: 'Ctrl + B',
    content: makeIcon('bold'),
    active: ed => ed.isActive('bold'),
    enabled: ed => ed.can().toggleBold(),
    action: ed => ed.chain().focus().toggleBold().run()
})}
export const italicControls = {italic: makeButton({
    title: 'italic',
    shortcut: 'Ctrl + I',
    content: makeIcon('italic'),
    active: ed => ed.isActive('italic'),
    enabled: ed => ed.can().toggleItalic(),
    action: ed => ed.chain().focus().toggleItalic().run()
})}
export const strikeControls = {strike: makeButton({
    title: 'strike',
    shortcut: 'Ctrl + Shift + S',
    content: makeIcon('strike'),
    active: ed => ed.isActive('strike'),
    enabled: ed => ed.can().toggleStrike(),
    action: ed => ed.chain().focus().toggleStrike().run()
})}
export const underlineControls = {underline: makeButton({
    title: 'underline',
    shortcut: 'Ctrl + U',
    content: makeIcon('underline'),
    active: ed => ed.isActive('underline'),
    enabled: ed => ed.can().toggleUnderline(),
    action: ed => ed.chain().focus().toggleUnderline().run()
})}
export const subscriptControls = {subscript: makeButton({
    title: 'subscript',
    shortcut: 'Ctrl + ,',
    content: makeIcon('subscript'),
    active: ed => ed.isActive('subscript'),
    enabled: ed => ed.can().toggleSubscript(),
    action: ed => ed.chain().focus().toggleSubscript().run()
})}
export const superscriptControls = {superscript: makeButton({
    title: 'superscript',
    shortcut: 'Ctrl + .',
    content: makeIcon('superscript'),
    active: ed => ed.isActive('superscript'),
    enabled: ed => ed.can().toggleSuperscript(),
    action: ed => ed.chain().focus().toggleSuperscript().run()
})}
export const blockquoteControls = {blockQuote: makeButton({
    title: 'blockquote',
    shortcut: 'Ctrl + Shift + B',
    content: makeIcon('blockquote'),
    active: ed => ed.isActive('blockquote'),
    enabled: ed => ed.can().toggleBlockquote(),
    action: ed => ed.chain().focus().toggleBlockquote().run()
})}
export const bulletListControls = {bulletList: makeButton({
    title: 'bullet list',
    shortcut: 'Ctrl + Shift + 8',
    content: makeIcon('bulletList'),
    active: ed => ed.isActive('bulletList'),
    enabled: ed => ed.can().toggleBulletList(),
    action: ed => ed.chain().focus().toggleBulletList().run()
})}
export const orderedListControls = {orderedList: makeButton({
    title: 'ordered list',
    shortcut: 'Ctrl + Shift + 7',
    content: makeIcon('orderedList'),
    active: ed => ed.isActive('orderedList'),
    enabled: ed => ed.can().toggleOrderedList(),
    action: ed => ed.chain().focus().toggleOrderedList().run()
})}
