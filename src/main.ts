import { createApp } from 'vue'
import './styles-te.css'
import './styles-tec.css'
import './demo-styles.css'
import App from './App.vue'

createApp(App).mount('#app')
