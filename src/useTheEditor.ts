

import { TheEditor } from './TheEditor'
import type { TheEditorExtensionList } from './TheEditor'
import { watch, ref, onMounted, onBeforeMount } from 'vue'

export type TheEditorComponentProps = {
    modelValue: string
    toolbar?: string|string[]
    disable?: string[]
    enable?: string[]
    extensions?: TheEditorExtensionList
    toolbar_classes?: string
    content_classes?: string
}

type Emits = {
    (e: 'update:modelValue', value: string): void
    (e: 'loaded', value: TheEditor): void
}

export default (props:TheEditorComponentProps, emits: Emits) => {
    const editor = ref<TheEditor>()

    watch(() => props.modelValue, (value) => {
        if (!editor.value) return
        const isSame = editor.value.getHTML() === value
        if (isSame) return
        editor.value.commands.setContent(value, false)
    })

    onMounted(() => {
        editor.value = new TheEditor({
            content: props.modelValue,
            bar_layout: props.toolbar,
            disable_default_extensions: props.disable,
            enable_default_extensions: props.enable,
            extensions: props.extensions,
            onUpdate: () => {
                emits('update:modelValue', editor.value?.getHTML() || '')
            }
        })

        emits('loaded', editor.value)
    })

    onBeforeMount(() => {
        editor.value?.destroy()
        editor.value = undefined
    })

    return {editor}
} 