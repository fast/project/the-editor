import {
    mergeAttributes,
    Node as TipTapNode,
} from '@tiptap/core'

import { VueNodeViewRenderer } from '@tiptap/vue-3'
import ImageComponent from './Image.vue'
import ImageTools from '../components/ImageTools.vue'
import { Plugin, PluginKey, } from 'prosemirror-state'
import { DOMParser as PMDOMParser} from 'prosemirror-model'
import type { TheEditor } from '../TheEditor'

export interface ImageOptions {
    FigureHTMLAttributes: Record<string, any>,
    ImageHTMLAttributes: Record<string, any>,
    uploadImage: (src: File | string) => Promise<string>,
    openImageDialog?: (editor: TheEditor) => void,
    renderSrc?: (src: string) => Promise<string>,
}

declare module '@tiptap/core' {
    interface Commands<ReturnType> {
        image: {
            /**
             * Add an image
             */
            setImage: (options: { src: string, alt?: string, caption?: string, width?: number }) => ReturnType,
        }
    }
}

// export const inputRegex = /(?:^|\s)(!\[(.+|:?)]\((\S+)(?:(?:\s+)["'](\S+)["'])?\))$/




const elementIsFigure = (element: HTMLElement) => element.tagName === 'FIGURE'

export const Image = TipTapNode.create<ImageOptions>({
    name: 'image',
    atom: true,
    addOptions() {
        return {
            FigureHTMLAttributes: {
                class: 'tec-figure-image'
            },
            ImageHTMLAttributes: {},
            uploadImage: () => Promise.resolve(''),
        }
    },

    inline() {
        return true
    },

    group() {
        return 'inline'
    },

    draggable: true,

    addAttributes() {
        return {
            src: {
                parseHTML: elm => {
                    if (elementIsFigure(elm)) {
                        return elm.querySelector('img')?.getAttribute('src') || ''
                    }
                    return elm.getAttribute('src') || ''

                }
            },
            alt: {
                parseHTML: elm => {
                    if (elementIsFigure(elm)) {
                        return elm.querySelector('img')?.getAttribute('alt') || null
                    }
                    return elm.getAttribute('alt') || null
                }
            },
            caption: {
                parseHTML: elm => {
                    if (elementIsFigure(elm)) {
                        return elm.querySelector('figcaption')?.textContent || null
                    }
                    return elm.getAttribute('title') || null
                }
            },
            width: {
                default: 50,
                parseHTML: elm => {
                    if (elementIsFigure(elm)) {
                        let width = elm.style.width
                        // If width is a percent, return as an integer, clamped between 10 and 100
                        if (width && width.includes('%')) {
                            return Math.max(10, Math.min(100, parseInt(width)))
                        }
                    }
                    return 50
                }
            },
        }
    },

    parseHTML() {
        return [
            {
                tag: 'img[src]'
            },
            {
                tag: 'figure:has(img)',
            },
        ]
    },

    renderHTML({ HTMLAttributes }) {
        const { caption, width, alt, src, ...rest } = HTMLAttributes
        return [
            'figure', mergeAttributes({style: `width: ${width}%;` }, this.options.FigureHTMLAttributes, rest),
            ['img', mergeAttributes(this.options.ImageHTMLAttributes, {src, alt})],
            ['figcaption', {}, caption || '']
        ]
    },

    addCommands() {
        return {
            setImage: options => ({ commands }) => {
                return commands.insertContent({
                    type: this.name,
                    attrs: options,
                })
            },
        }
    },

    addNodeView() {
        // @ts-ignore
        return VueNodeViewRenderer(ImageComponent)
    },

    addProseMirrorPlugins(this) {
        const self = this
        return [
            new Plugin({
                key: new PluginKey('imageIntercept'),
                props: {
                    handlePaste(view, event, _slice) {
                        event.preventDefault()
                        let html = event.clipboardData?.getData('text/html')
                        if (!html) {
                            return false
                        }
                        // TODO: Handle pasted image files (can do multiple!)
                        let parser = new DOMParser()
                        let doc = parser.parseFromString(html, 'text/html')

                        let do_all = Promise.all(Array.from(doc.querySelectorAll('img')).map(async img => {
                            img.src = await self.options.uploadImage(img.src)
                        }))

                        do_all.then(() => {
                            let out = PMDOMParser.fromSchema(view.state.schema).parse(doc.documentElement)
                            let tr = view.state.tr.replaceSelectionWith(out)
                            view.dispatch(tr)
                        })
                        return true
                    },
                }
            })
        ]
    }
})


export const imageControls = { image: ImageTools }


export default function(config?: Partial<ImageOptions>){
    return {
        extension: Image.configure(config),
        controls: imageControls,
    }
}