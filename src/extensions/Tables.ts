// Combines and groups all the table extensions into one extension

import { Extension, mergeAttributes } from "@tiptap/core"
import { Table, TableOptions} from '@tiptap/extension-table'
import { TableCell, TableCellOptions } from '@tiptap/extension-table-cell'
import { TableHeader, TableHeaderOptions } from '@tiptap/extension-table-header'
import { TableRow, TableRowOptions } from '@tiptap/extension-table-row'

export interface TablesOptions {
    table: Partial<TableOptions>,
    table_cell: Partial<TableCellOptions>,
    table_header: Partial<TableHeaderOptions>,
    table_row: Partial<TableRowOptions>,
}

export const Tables = Extension.create<TablesOptions>({
    name: 'tables',

    addExtensions(){
        return [
            Table.configure({...this.options.table, HTMLAttributes: mergeAttributes({class: 'tec-table'}, this.options.table.HTMLAttributes??{})}),
            TableCell.configure(this.options.table_cell),
            TableHeader.configure(this.options.table_header),
            TableRow.configure(this.options.table_row),
        ]
    }
})