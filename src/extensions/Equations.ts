import { Node, mergeAttributes } from '@tiptap/core'
import type { TheEditor } from '../TheEditor'
import EquationTools from '../components/EquationTools.vue'

declare module '@tiptap/core' {
    interface Commands<ReturnType> {
      equation: {
        /**
         * Add an image
         */
        addEquation: (options: {raw: string, display_mode?:boolean}) => ReturnType,
      }
    }
  }

interface EquationOptions {
    HTMLAttributes: Record<string, any>
    openEquationDialog?: (editor: TheEditor, current?: {raw: string, display_mode: boolean}) => void,
    renderDisplay?: (src: string, display_mode?: boolean) => string,
    renderStore?: (src: string, display_mode?: boolean) => string,
}


export const Equation  = Node.create<EquationOptions>({
    name: 'equation',
    group: 'inline',
    inline: true,
    content: '',
    atom: true,
    selectable: true,
    draggable: true,
    parseHTML(){
        return [
            {
                tag: 'span[is-equation]',
            },
        ]
    },
    addOptions(){
        return {
            HTMLAttributes: {},
            // @ts-ignore
            renderDisplay: (src: string, display_mode=false) => src
        }
    },
    renderHTML({HTMLAttributes}) {
        let o = document.createElement('span')
        let attributes = mergeAttributes(this.options.HTMLAttributes, {
            'data-raw': encodeURIComponent(HTMLAttributes.raw),
            'is-equation': "1",
        })
        
        if (HTMLAttributes.display_mode){
            attributes['data-display-mode'] = '1'
        }

        Object.keys(attributes).forEach(key => {
            o.setAttribute(key, attributes[key])
        })
        let render_fn = this.options.renderStore || this.options.renderDisplay
        o.innerHTML = render_fn!(HTMLAttributes.raw, HTMLAttributes.display_mode)
        return o
    },
    addAttributes(){
        return{
            // rendered: {
            //     parseHTML: elm => {
            //         console.log(elm)
            //         // let v  = elm.getAttribute('rendered')
            //         // if (v === '' || v === undefined || v === null){
            //         //     v = this.options.renderEquation!(elm.getAttribute('data-raw') || 'Missing Equation')
            //         // }
            //         // return v
            //         return this.options.renderEquation!(elm.getAttribute('data-raw') || 'Missing Equation')
            //     }
            // },
            raw: {
                parseHTML: elm => {
                    return decodeURIComponent(elm.getAttribute('data-raw')||'')
                }
            },
            display_mode:{
                default: false,
                parseHTML: elm => {
                    return elm.hasAttribute('data-display-mode')
                }
            }
        }
    },
    addNodeView(){
        return ({node, extension}) => {
            const {options} = extension
            //const {view} = editor
            const dom = document.createElement('span')
            dom.setAttribute('is-equation', '1')
            //dom.innerHTML = node.attrs.rendered
            dom.innerHTML = options.renderDisplay(node.attrs.raw, node.attrs.display_mode)
            return {dom}
        }
    },

    addCommands(){
        return{
            addEquation: options => ({chain}) => {
                return chain()
                .insertContent({
                    type: this.name,
                    attrs: {
                        ...options,
                    }
                })
                .run()
            }
        }
    }
})

export const equationControls = { equation: EquationTools }


export default function(config?: Partial<EquationOptions>){
    return {
        extension: Equation.configure(config),
        controls: equationControls,
    }
}