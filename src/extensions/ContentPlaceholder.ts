import {Node, mergeAttributes} from '@tiptap/core'
import { VueNodeViewRenderer } from '@tiptap/vue-3'
import ContentPlaceholderView from './ContentPlaceholder.vue'
import ContentPlaceholderControls from '../components/ContentPlaceholder.vue'

declare module '@tiptap/core'{
    interface Commands<ReturnType>{
        contentPlaceholder: {
            addContentPlaceholder: () => ReturnType
        }
    }
}

interface ContentPlaceholderOptions{
    HTMLAttributes: Record<string, any>
    options: Array<({
        label: string
        value: string
    } | {
        group: string
        options: Array<{
            label: string
            value: string
        }>
    })>
}

export const ContentPlaceholder = Node.create<ContentPlaceholderOptions>({
    name: 'contentPlaceholder',
    group: 'inline',
    inline: true,
    content: '',
    atom: true,

    parseHTML(){
        return [
            {tag: 'span[data-placeholder]'}
        ]
    },
    addAttributes(){
        return {
            value: {
                default: '',
                parseHTML: (elm:HTMLElement) => elm.innerText
            }
        }
    },

    addOptions(){
        return {
            options: [{label: "Example", value: "{{example}}"}, {group: 'Group 1', options: [{label: "Example 2", value: "{{example2}}"}]}],
            HTMLAttributes: {
                class: null,
            },
        }
    },

    renderHTML({HTMLAttributes}: any){
        let {value, ...rest} = HTMLAttributes
        return ['span', mergeAttributes(
            this.options.HTMLAttributes,
            {
            'data-placeholder': "1",
            ...rest,
        }), value]
    },

    addNodeView(){
        // Don't know why this broke honestly
        // @ts-ignore
        return VueNodeViewRenderer(ContentPlaceholderView)
    },

    addCommands(){
        return{
            addContentPlaceholder: () => ({chain}) => {
                return chain().insertContent({
                    type: this.name,
                    attrs: {value: ''}
                }).run()
            }
        }
    },
})


export const contentPlaceholderControls = {contentPlaceholder: ContentPlaceholderControls}

export default function (config?: Partial<ContentPlaceholderOptions>){
    return {
        extension: ContentPlaceholder.configure(config),
        controls: contentPlaceholderControls,
    }
}
