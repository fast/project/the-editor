import {h} from "vue"
import icons from "./icons"
import LazySVGVue from "./components/LazySVG.vue"

export default (icon_name: keyof typeof icons, props?: Record<string, any>) => h(LazySVGVue, {...props, xml: icons[icon_name]})