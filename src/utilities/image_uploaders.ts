const getFileExtensionFromBlob = (blob: Blob) => {
    const mimeType = blob.type;
    const extensionMap: Record<string, string> = {
        'image/jpeg': 'jpg',
        'image/png': 'png',
        'image/gif': 'gif',
    };
    return extensionMap[mimeType] || '';
}

var ALPHABET = '0123456789abcdefghijklmnopqrstuvwxyz';
var ID_LENGTH = 8;

function generateUniqueId() {
    var result = '';
    for (var i = 0; i < ID_LENGTH; i++) {
        result += ALPHABET.charAt(Math.floor(Math.random() * ALPHABET.length));
    }
    return result;
}

const blobToFile = (blob: Blob, filename?: string) => {
    const extension = getFileExtensionFromBlob(blob);
    const name = filename || generateUniqueId();
    return new File([blob], `${name}.${extension}`, { type: blob.type });
}

const blobURL = (blob: Blob) => URL.createObjectURL(blob)

const dataURLtoBlob = (dataURL: string) => {
    let blob: Blob
    let parts = dataURL.split(',')
    let mime = parts[0].match(/:(.*?);/)![1]
    if (parts[0].indexOf('base64') !== -1) {
        var bstr = atob(parts[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        blob = new Blob([u8arr], { type: mime });
    } else {
        var raw = decodeURIComponent(parts[1]);
        blob = new Blob([raw], { type: mime });
    }
    return blob
}
const dataURLtoBlobURL = (dataURL: string) => blobURL(dataURLtoBlob(dataURL))

const fileToDataURL = (file: File | Blob) => new Promise<string>((resolve, reject) => {
    const reader = new FileReader()
    reader.onloadend = () => resolve(reader.result as string)
    reader.onerror = reject
    reader.readAsDataURL(file)
})

const fileToBlobURL = (file: File) => {
    let blob = new Blob([file], { type: file.type })
    return blobURL(blob)
}


export const inMemoryImageUploader = (options?: {
    // Loads images as in-memory blobs that won't persist after the page is closed
    // Keeps URLs tight, can deal with blobs later if desired
    fetchURLs?: boolean
}) => async (src: File | string) => {
    // Convert the image to a blob and return a blob url
    if (src instanceof File) {
        return fileToBlobURL(src)
    }

    if (src.startsWith('data:')) {
        return dataURLtoBlobURL(src)
    }

    if (src.startsWith('blob:')) {
        return src
    }

    if (options?.fetchURLs) {
        // Fetch the image and return a blob url
        let response = await fetch(src)
        let blob = await response.blob()
        return URL.createObjectURL(blob)
    }
    return src
}

export const base64ImageUploader = (options?: {
    fetchURLs?: boolean
}) => async (src: File | string) => {
    // Convert the image to a blob and return a blob url
    if (src instanceof File) {
        return await fileToDataURL(src)
    }

    if (src.startsWith('data:')) {
        return src
    }

    if (src.startsWith('blob:') || options?.fetchURLs) {
        let response = await fetch(src)
        let blob = await response.blob()
        return await fileToDataURL(blob)
    }

    return src
}

export const serverImageUploader = (options: {
    fetchURLs?: boolean,
    handleUpload: (file: File) => Promise<string>
}) => async (src: File | string) => {
    if (src instanceof File) {
        return await options.handleUpload(src)
    }
    if (src.startsWith('data:')) {
        return await options.handleUpload(blobToFile(dataURLtoBlob(src)))
    }
    if (src.startsWith('blob:')) {
        return await options.handleUpload(blobToFile(await fetch(src).then(r => r.blob())))
    }
    if (options.fetchURLs) {
        // get the filename from the url
        let filename = src.split('/').pop() || undefined
        let response = await fetch(src)
        let blob = await response.blob()
        return await options.handleUpload(blobToFile(blob, filename))
    }
    return src
}