import {expect, test} from 'vitest'
import {
    base64ImageUploader,
    serverImageUploader,
    inMemoryImageUploader,
} from './image_uploaders'


const fakeImage = new File([''], 'image.jpg', { type: 'image/jpeg' })
const fakeDataURL = 'data:image/jpeg;base64,'
const fakeBlob = new Blob([''], { type: 'image/jpeg' })
const fakeBlobURL = URL.createObjectURL(fakeBlob)
const fakeURL = '/image.jpg'
const fakeImageHandler = async (_file: File) => {
    return fakeURL
}

const b64_no_fetch = base64ImageUploader()
//const b64_fetch = base64ImageUploader({ fetchURLs: true })
const server_no_fetch = serverImageUploader({ handleUpload: fakeImageHandler })
//const server_fetch = serverImageUploader({ fetchURLs: true, handleUpload: fakeImageHandler })
const memory_no_fetch = inMemoryImageUploader()
//const memory_fetch = inMemoryImageUploader({ fetchURLs: true })

test('base64ImageUploader', async () => {
    expect(await b64_no_fetch(fakeImage)).toBe(fakeDataURL)
    expect(await b64_no_fetch(fakeDataURL)).toBe(fakeDataURL)
    expect(await b64_no_fetch(fakeBlobURL)).toBe(fakeDataURL)
    expect(await b64_no_fetch(fakeURL)).toBe(fakeURL)
})

test('serverImageUploader', async () => {
    expect(await server_no_fetch(fakeImage)).toBe(fakeURL)
    expect(await server_no_fetch(fakeDataURL)).toBe(fakeURL)
    expect(await server_no_fetch(fakeBlobURL)).toBe(fakeURL)
    expect(await server_no_fetch(fakeURL)).toBe(fakeURL)
})

test('inMemoryImageUploader', async () => {
    expect(await memory_no_fetch(fakeImage)).toMatch(/^blob:/)
    expect(await memory_no_fetch(fakeImage)).toMatch(/^blob:/)
    expect(await memory_no_fetch(fakeDataURL)).toMatch(/^blob:/)
    expect(await memory_no_fetch(fakeURL)).toBe(fakeURL)
})