const layout = [
    'history',
    '|',
    'styleSelector',
    '|',
    'bold',
    'italic',
    'underline',
    'strike',
    'superscript',
    'subscript',
    '|',
    'bulletList',
    'orderedList',
    'blockquote',
    '|',
    'textAlign',
    '|',
    'table',
    'link'
]

export const defaultToolbarArray = layout
export const defaultToolbarString = layout.join(' ')