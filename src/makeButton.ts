import { computed, DefineComponent, defineComponent, h, VNode, PropType} from "vue";
import { TheEditor } from "./TheEditor";
import EditBarButtonVue from "./components/TheEditBarButton.vue";


function makeButton(options: {
    // Returns a lovely button component with a TheEditor instance injected
    active?: (editor:TheEditor)=>boolean,
    enabled?: (editor:TheEditor)=>boolean,
    action: (editor: TheEditor)=>void,
    title: string,
    shortcut?: string,
    content: VNode|string
}): DefineComponent{
    return defineComponent({
        props: {
            editor: {
                type: Object as PropType<TheEditor>,
                required: true,
            }
        },
        setup(props){
            const props_t = props as {editor: TheEditor}
            return {
                active: computed(()=>options.active ? options.active(props_t.editor!) : false),
                enabled: computed(()=>options.enabled? options.enabled(props_t.editor!): true),
                action: ()=>options.action(props_t.editor!),
                title: options.title,
                shortcut: options.shortcut,
                content: options.content
            }
        },
        render(){
            return h(EditBarButtonVue, {
                action: this.action,
                enabled: this.enabled,
                active: this.active,
                title: this.title,
                shortcut: this.shortcut
            }, ()=>this.content)
        }
    })
}

export default makeButton