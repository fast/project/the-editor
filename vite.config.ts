import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import dts from 'vite-plugin-dts'
import { copyFileSync } from 'fs'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    dts({
      include: ['lib'],
      rollupTypes: true
    }),
    {
      name: 'copy-content-styles',
      writeBundle() {
        copyFileSync('src/styles-tec.css', 'dist/styles-tec.css')
      }
    }
  ],
  define: { 'process.env': {} },
  build: {
    minify: true,
    lib: {
      entry: ['lib/main.ts'],
      name: 'TheEditor',
      fileName: (format) => `main.js`,
      formats: ['es'],
    },
    rollupOptions: {
      external: ['vue'],
      output: {
        globals: {
          vue: 'Vue'
        },
        assetFileNames: (assetInfo) => {
          if (assetInfo.name === 'style.css') {
            return 'styles-te.css'
          }
        }
      },
    }
  }
})
